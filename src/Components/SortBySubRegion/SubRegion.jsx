import './SubRegion.css'

export default function SortBySubRegion({dataoriginal,func,regiond}){


    const presentSubRegions=[]
    dataoriginal.map((object)=>{
        if(!presentSubRegions.includes(object.subregion) && object.region===regiond){
            presentSubRegions.push(object.subregion)
        }
    })
    const optionsTag=presentSubRegions.map((string,index)=>{
        return <option key={index} value={string}>{string}</option>
    })

    function subRegion(e){
        func(e.target.value)
    }

    return(
        <select onClick={subRegion} className="select1">
            <option value="">Filter By SubRegions</option>
            {optionsTag}
        </select>
    )
}