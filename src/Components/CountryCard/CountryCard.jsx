import { Link } from 'react-router-dom'
import React from 'react'
import './CountryCard.css'
import { ThemeContext } from '../../App';

export default function CountryCard({ data }) {

    const {darkMode,setDarkMode}=React.useContext(ThemeContext)

    let textColor=darkMode ? "darkTextColors" : "";

    let creatingUi = data.map((object, index) => {
        return (
            <Link to={`/countrydetails/${object.name.common}`} className='link1' key={index}>
                <div className="imgIndividual" key={index}>
                    <img src={object.flags.png} className="img1" />
                    <div className={`titleContainer ${textColor}`}>
                        <h1 className='h1'>{object.name.common}</h1>
                        <h3>Population :<span className="span">{object.population}</span></h3>
                        <h3>Region :<span className="span">{object.region}</span></h3>
                        <h3>Capital :<span className="span">{object.capital}</span></h3>
                        <h3>SubRegion :<span className="span">{object.subregion}</span></h3>
                        <h3>Area :<span className="span">{object.area}</span></h3>
                    </div>
                </div>
            </Link>
        )
    })

    return (
        <div className="grid">
            {creatingUi}
        </div>
    )
}

// `${object.capital}`