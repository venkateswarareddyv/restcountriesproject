export default function SortGroup({groupFunc}) {

    function toGetValue(e){
        groupFunc(e.target.value)
    }

    return (
        <select className="select" onClick={toGetValue}>
            <option value="">--Sort by--</option>
            <option value="name">Name</option>
            <option value="area">Area</option>
            <option value="population">population</option>
        
        </select>
    )
}