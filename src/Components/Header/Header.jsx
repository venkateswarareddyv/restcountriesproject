import React from "react"
import moon from '../../assets/moon-regular.svg'
import whiteMoon from '../../assets/whitemoon.png'
import { ThemeContext } from '../../App';

import './Header.css'

export default function Header(){

    const {darkMode,setDarkMode}=React.useContext(ThemeContext)

    function ChangeState(){
        setDarkMode((prev)=>{
            return !prev
        })
    }

    let backgroundColor= darkMode? "background" : ""
    let image=darkMode ? whiteMoon : moon;

    return(
        <header className={`head ${backgroundColor}`}>
            <h1 className="headingstyles">Where in the world?</h1>
            <div className="imgContainer" onClick={ChangeState}>
                <img src={image} className="img"/>
                <label className="label">Dark Mode</label>
            </div>
        </header>
    )
}