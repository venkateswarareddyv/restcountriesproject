import React from 'react'
import { Link, useParams } from 'react-router-dom'
import fetching from '../FetchingResource/FetchingResource.js'
import backArrow from '../../assets/backArrow.svg'
import { TailSpin } from 'react-loader-spinner';
import { ThemeContext } from '../../App';
import './home.css'

export default function Home() {
    let { countryName } = useParams()

    const {darkMode,setDarkMode}=React.useContext(ThemeContext)

    const [countryData, setCountryData] = React.useState([])

    React.useEffect(() => {
        async function toGetIndividual() {
            const data = await fetching(`https://restcountries.com/v3.1/name/${countryName}`);
            setCountryData(data)
        }
        toGetIndividual();
    }, [])

    let individualBackground=darkMode ? "individualBackground" : "";
    let textColor=darkMode ? "individualText" : "" ;
    let bacButton=darkMode ? "backButton" : "";

    return (
        <div className={`${individualBackground}`}>
            {countryData.length === 0 ? <div className='spinner'><TailSpin
                height="80"
                width="80"
                color="#4fa94d"
                ariaLabel="tail-spin-loading"
                radius="1"
                wrapperStyle={{}}
                wrapperClass=""
                visible={true}
            /></div> :
                <div className='back'>
                    <div>
                        <Link to="/"><button className={`button ${bacButton}`}>
                            <img src={backArrow} alt='backArrow' className='backArrowImg' />Back
                        </button></Link>
                    </div>
                    <div className={`imgIndi ${textColor}`}>
                        <img src={countryData[0].flags.svg} alt="IndividualImage" className='countyImage' />
                        <div>
                            <h1>{countryData[0].name.common}</h1>
                            <div className='titleIndi'>
                                <div>
                                    <h3>Native Name :<span className="span">{countryData[0].name.official}</span></h3>
                                    <h3>Population :<span className="span">{countryData[0].population}</span></h3>
                                    <h3>Region :<span className="span">{countryData[0].region}</span></h3>
                                    <h3>Sub Region :<span className="span">{countryData[0].subregion}</span></h3>
                                    <h3>Capital :<span className="span">{countryData[0].capital[0]}</span></h3>
                                </div>
                                <div className='m'>
                                    <h3>Top Level Domain :<span className="span">{countryData[0].tld}</span></h3>
                                    <h3>Curriencies :<span className="span">{Object.values(countryData[0].currencies)[0].name}</span></h3>
                                    <h3>Languages :<span className="span">{Object.values(countryData[0].languages)}</span></h3>
                                </div>
                            </div>
                            {countryData[0].borders && <div>
                                <label>Border Countries :</label>
                                {countryData[0].borders.map((boundry) => {
                                    return <button>{boundry}</button>
                                })}
                            </div>}
                        </div>
                    </div>
                </div>

            }

        </div>
    )
}