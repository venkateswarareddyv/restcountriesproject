import axios from 'axios';
 
 export default async function fetching(url){
    const data1=axios.get(url).then((response)=>{
        return response.data
    })
    return data1;
}