export default function SortByRegion({regFunc}) {

    

    return (
        <select className="select" onClick={regFunc}>
            <option value="">Filter By region</option>
            <option value="Africa">Africa</option>
            <option value="Americas">Americas</option>
            <option value="Asia">Asia</option>
            <option value="Europe">Europe</option>
            <option value="Oceania">Oceania</option>
        </select>
    )
}
