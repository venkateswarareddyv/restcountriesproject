import React from 'react'
import fetching from '../FetchingResource/FetchingResource';
import SortByRegion from '../SortByRegion/SortByRegion';
import SortBySubRegion from '../SortBySubRegion/SubRegion';
import SortGroup from '../SortGroup/SortGroup';
import CountryCard from '../CountryCard/CountryCard';
import { TailSpin } from 'react-loader-spinner';
import { ThemeContext } from '../../App';
import './Country.css'

export default function Countries() {
    const {darkMode,setDarkMode}=React.useContext(ThemeContext)

    const [countryData, changeCountryData] = React.useState([]);

    const [search, changeSearch] = React.useState('')

    const [region, changeRegion] = React.useState('')

    const [state, basedOnSubRegion] = React.useState('');

    const [groupAsp, basedOngroup] = React.useState('');

    React.useEffect(() => {
        async function toGetData() {
            const data = await fetching('https://restcountries.com/v3.1/all');
            changeCountryData(data);
        }
        toGetData()
    }, [])


    const searchInput = (e) => changeSearch(e.target.value)

    function basedOnRegion(e) {
        
        if (e.target.value == '') {
            changeRegion('')
        } else {
            changeRegion(e.target.value)
        }
    }

    let filterCountries = countryData.filter((eachCountry) => {
        if (eachCountry.subregion !== undefined) {
            if (eachCountry.region.includes(region) &&
                (eachCountry.name.common.toLowerCase().includes(search.toLowerCase()) &&
                    eachCountry.subregion.includes(state))
            ) {
                return eachCountry
            }

        }

    })

    filterCountries = filterCountries.sort((a, b) => {
        if (groupAsp === "population") {
            if (a.population < b.population) {
                return -1
            } else if (a.population > b.population) {
                return 1
            } else {
                return 0
            }
        } else if (groupAsp === "name") {
            if (a.name.common < b.name.common) {
                return -1
            } else if (a.name.common > b.name.common) {
                return 1
            } else {
                return 0
            }
        } else if (groupAsp === "area") {
            if (a.area < b.area) {
                return -1
            } else if (a.area > b.area) {
                return 1
            } else {
                return 0
            }
        }
    })

    let noData;

    if (filterCountries.length === 0) {
        noData = true;
    } else {
        noData = false;
    }

    let countriesBackground=darkMode ? "darkBodyBackground" : "";

    return (
        <div className={`main ${countriesBackground}`}>
            <div className="region">
                <input type="text" className="input icon" placeholder="Search by country" onChange={searchInput} />
                <div>
                    <SortByRegion regFunc={basedOnRegion} />
                    <SortBySubRegion dataoriginal={countryData} func={basedOnSubRegion} regiond={region} />
                    <SortGroup groupFunc={basedOngroup} />
                </div>
            </div>
            <div className="country">
                {countryData.length === 0 ? <div className='spinner'><TailSpin
                    height="80"
                    width="80"
                    color="#4fa94d"
                    ariaLabel="tail-spin-loading"
                    radius="1"
                    wrapperStyle={{}}
                    wrapperClass=""
                    visible={true}
                /></div>:
                    <div>
                        <CountryCard data={filterCountries} />
                        {noData && <p className="p">No Match Data Found</p>}
                    </div>
                }
            </div>

        </div>
    )
}