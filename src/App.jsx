import { BrowserRouter, Route, Routes } from 'react-router-dom'
import React from 'react'
import Header from "./Components/Header/Header.jsx";
import Countries from "./Components/Country/Country.jsx";
import Home from './Components/Home/home.jsx';
import './App.css'

export const ThemeContext = React.createContext();


function App() {

  const [darkMode, setDarkMode] = React.useState(false)

  return (
    <div>
      <ThemeContext.Provider value={{ darkMode, setDarkMode }}>
      <BrowserRouter>
        <Header />
        <Routes>
          <Route exact path='/' element={<Countries />} />
          <Route exact path="/countrydetails/:countryName" element={<Home />} />
        </Routes>
      </BrowserRouter>
    </ThemeContext.Provider>
    </div >
  )
}

export default App
